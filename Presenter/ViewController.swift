//
//  ViewController.swift
//  Presenter
//
//  Created by Will on 22/02/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit
import CircleMenu
import ChameleonFramework

extension UIColor {
    static func color(_ red: Int, green: Int, blue: Int, alpha: Float) -> UIColor {
        return UIColor(
            colorLiteralRed: Float(1.0) / Float(255.0) * Float(red),
            green: Float(1.0) / Float(255.0) * Float(green),
            blue: Float(1.0) / Float(255.0) * Float(blue),
            alpha: alpha)
    }
}

class ViewController: UIViewController, CircleMenuDelegate {
    
    var button: CircleMenu!
    
    let items: [(icon: String, color: UIColor)] = [
        ("Resume", UIColor(red:0.60, green:0.75, blue:0.91, alpha:1.0)),
        ("Work", UIColor(red:0.04, green:0.68, blue:0.85, alpha:1.0)),
        ("Approval", UIColor(red:0.22, green:0.37, blue:0.66, alpha:1.0)),
        ("Work", UIColor(red:0.04, green:0.68, blue:0.85, alpha:1.0)),
        ("nearby-btn", UIColor(red:1, green:0.39, blue:0, alpha:1)),
        ]
    
    @IBOutlet weak var img_background: UIImageView!
    
    @IBOutlet weak var btn_settings: UIBarButtonItem!
    @IBOutlet weak var lbl_home: UILabel!
    @IBOutlet weak var img_willPricecheck: UIImageView!
    @IBOutlet weak var img_willHallam: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        button = CircleMenu(
            frame: CGRect(x: 498, y: 259, width: 65, height: 65),
            normalIcon:"dot_logos",
            selectedIcon:"icon_close",
            buttonsCount: 4,
            duration: 1,
            distance: 165)

        button.backgroundColor = UIColor.lightGray
        button.delegate = self
        button.layer.cornerRadius = button.frame.size.width / 2.0

        lbl_home.alpha = 0
        button.alpha = 0
        img_willPricecheck.alpha = 0
        img_willHallam.alpha = 0
        
        view.addSubview(button)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.object(forKey: "SettingsToggle") == nil {
            
            UserDefaults.standard.set("Blue", forKey: "SettingsToggle")
            img_background.image = UIImage(named: "PricecheckBackground")
            img_willPricecheck.isHidden = false
            img_willHallam.isHidden = true
        }else{
            
            switch (UserDefaults.standard.value(forKey: "SettingsToggle") as! String) {
                
            case "Blue":
                
                img_background.image = UIImage(named: "PricecheckBackground")
                navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
                img_willPricecheck.isHidden = false
                img_willHallam.isHidden = true
            case "Pink":
                
                img_background.image = UIImage(named: "HallamBackground")
                navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
                img_willPricecheck.isHidden = true
                img_willHallam.isHidden = false
            default:
                
                break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        UIView.animate(withDuration: 1, animations: {
            
            self.lbl_home.alpha = 1
        }) {(true) in
            
            UIView.animate(withDuration: 1, animations: {
                
                self.img_willHallam.alpha = 1
                self.img_willPricecheck.alpha = 1
                self.button.alpha = 1
            })
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        button.hideButtons(1)
    }
    
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        
        button.backgroundColor = items[atIndex].color
        
        if img_willPricecheck.isHidden {
            
            if atIndex == 1 {
                
                button.isHidden = true
            }
        }else{
            
            if atIndex == 3 {
                
                button.isHidden = true
            }
        }
        
        button.setImage(UIImage(named: items[atIndex].icon), for: .normal)
        
        // set highlighted image
        let highlightedImage  = UIImage(named: items[atIndex].icon)?.withRenderingMode(.alwaysTemplate)
        
        button.setImage(highlightedImage, for: .highlighted)
        button.tintColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        
        switch atIndex {
            
        case 0:
            performSegue(withIdentifier: "myRole", sender: self)
            
        case 1:
            performSegue(withIdentifier: "myProjects", sender: self)
            
        case 2:
            performSegue(withIdentifier: "mySummary", sender: self)
        
        case 3:
            performSegue(withIdentifier: "myProjects", sender: self)
            
        default:
            print("Out Of Bounds")
        } 
    }
}

