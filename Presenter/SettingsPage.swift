//
//  SettingsPage.swift
//  Presenter
//
//  Created by Will on 01/04/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit
import RAMPaperSwitch

class SettingsPage: UIViewController {
    
    @IBOutlet weak var view_blue: UIView!
    @IBOutlet weak var view_pink: UIView!
    
    @IBOutlet weak var img_background: UIImageView!
    @IBOutlet weak var lbl_pricecheck: UILabel!
    @IBOutlet weak var lbl_pricecheckFlick: UILabel!
    @IBOutlet weak var lbl_hallam: UILabel!
    @IBOutlet weak var lbl_hallamFlick: UILabel!
    @IBOutlet weak var lbl_settingsTitle: UILabel!
    @IBOutlet weak var btn_goHome: UIButton!
    
    @IBOutlet weak var sch_blue: RAMPaperSwitch!
    @IBOutlet weak var sch_pink: RAMPaperSwitch!
    
    let proj = ProjectsTable()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_settingsTitle.alpha = 0
        view_blue.alpha = 0
        view_pink.alpha = 0
        btn_goHome.alpha = 0
        
//        let leftButton =  UIBarButtonItem(image: UIImage(named: "Return"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.goBack))
//        leftButton.tintColor = proj.getTheme()
        
        //self.navigationItem.leftBarButtonItem = leftButton
        
        sch_pink.layer.borderColor = UIColor(colorWithHex: 0xB70D50).cgColor
        
        sch_blue.layer.borderColor = UIColor(colorWithHex: 0x395DA7).cgColor
        
        self.ConfigureViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
        UIView.animate(withDuration: 1, animations: {
                
            self.lbl_settingsTitle.alpha = 1
            self.btn_goHome.alpha = 1
        }) {(true) in
            
            UIView.animate(withDuration: 1, animations: {
                
                self.view_blue.alpha = 1
                self.view_pink.alpha = 1
            })
        }
    }
    
    @IBAction func toggleBlue(_ sender: RAMPaperSwitch) {
        
        if sch_blue.isOn {
            
            UserDefaults.standard.set("Blue", forKey: "SettingsToggle")
            
            navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
            
            img_background.image = UIImage(named: "PricecheckBackground")!
            lbl_pricecheck.textColor = UIColor.white
            lbl_pricecheckFlick.textColor = UIColor.white
            lbl_pricecheckFlick.text = "Activated"
            lbl_hallam.textColor = UIColor.black
            lbl_hallamFlick.textColor = UIColor.black
            lbl_hallamFlick.text = "Flick To Activate"
            
            sch_pink.setOn(false, animated: true)
            sch_pink.isEnabled = true
            sch_blue.isEnabled = false
        }
    }
    
    @IBAction func togglePink(_ sender: RAMPaperSwitch) {
        
        if sch_pink.isOn {
            
            UserDefaults.standard.set("Pink", forKey: "SettingsToggle")
            
            navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
            
            img_background.image = UIImage(named: "HallamBackground")!
            lbl_hallam.textColor = UIColor.white
            lbl_hallamFlick.textColor = UIColor.white
            lbl_hallamFlick.text = "Activated"
            lbl_pricecheck.textColor = UIColor.black
            lbl_pricecheckFlick.textColor = UIColor.black
            lbl_pricecheckFlick.text = "Flick To Activate"
            
            sch_blue.setOn(false, animated: true)
            sch_pink.isEnabled = false
            sch_blue.isEnabled = true
        }
    }
    
    func ConfigureViews(){
        
        switch UserDefaults.standard.value(forKey: "SettingsToggle") as! String {
            
        case "Blue":
            
            lbl_pricecheckFlick.textColor = UIColor.white
            lbl_pricecheck.textColor = UIColor.white
            lbl_pricecheckFlick.text = "Activated"
            
            img_background.image = UIImage(named: "PricecheckBackground")!
            
            sch_blue.setOn(true, animated: true)
        case "Pink":
            
            lbl_hallamFlick.textColor = UIColor.white
            lbl_hallam.textColor = UIColor.white
            lbl_hallamFlick.text = "Activated"
            
            img_background.image = UIImage(named: "HallamBackground")!
            sch_pink.setOn(true, animated: true)
        default:
            
            break
        }
    }
    
    @IBAction func goBack(){
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(colorWithHex: Int) {
        self.init(
            red: (colorWithHex >> 16) & 0xFF,
            green: (colorWithHex >> 8) & 0xFF,
            blue: colorWithHex & 0xFF
        )
    }
}
