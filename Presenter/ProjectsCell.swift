//
//  ProjectsCell.swift
//  Presenter
//
//  Created by Chloe Baker on 02/04/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit
import expanding_collection
import Cosmos

class ProjectsCell: BasePageCollectionCell {
    
    @IBOutlet weak var lbl_projectTitle: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var lbl_projectType: UILabel!
    @IBOutlet weak var lbl_difficulty: CosmosView!
    @IBOutlet weak var lbl_difficultyName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        lbl_projectTitle.layer.shadowRadius = 2
        lbl_projectTitle.layer.shadowOffset = CGSize(width: 0, height: 3)
        lbl_projectTitle.layer.shadowOpacity = 0.2
    }
}
