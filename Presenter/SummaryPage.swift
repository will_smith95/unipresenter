//
//  SummaryPage.swift
//  Presenter
//
//  Created by WILL SMITH on 16/07/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit

class SummaryPage: UIViewController {

    @IBOutlet weak var img_background: UIImageView!
    @IBOutlet weak var btn_home: UIButton!
    @IBOutlet weak var lbl_summary: UILabel!
    @IBOutlet weak var btn_settings: UIButton!
    @IBOutlet weak var lbl_information: UILabel!
    @IBOutlet weak var lbl_information2: UILabel!
    @IBOutlet weak var img_handshake: UIImageView!
    @IBOutlet weak var img_team: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbl_summary.alpha = 0
        lbl_information.alpha = 0
        lbl_information2.alpha = 0
        img_handshake.alpha = 0
        img_team.alpha = 0
        
        lbl_information.text = "I've really enjoyed my placement at Pricecheck. It's given me a great insight into software development for a thriving business."
        
        lbl_information2.text = "I've learnt a lot from my time here and I'm happy to say I've been offered a graduate role for Pricecheck and have accepted it."
        
        img_handshake.image = UIImage(named: "Handshake.png")
        
        img_team.image = UIImage(named: "Team.png")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        StartAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        SetBackground()
    }
    
    func SetBackground() {
        
        if UserDefaults.standard.value(forKey: "SettingsToggle") as! String == "Blue" {
            
            img_background.image = UIImage(named: "PricecheckBackground")
        }else{
            
            img_background.image = UIImage(named: "HallamBackground")
        }
    }
    
    func StartAnimating(){
        
        UIView.animate(withDuration: 1, animations: {
            
            self.lbl_summary.alpha = 1
        }) {(true) in
            
            UIView.animate(withDuration: 2, animations: {
                
                self.lbl_information.alpha = 1
            }) {(true) in
                
                UIView.animate(withDuration: 4, animations: {
                    
                    self.lbl_information2.alpha = 1
                }) {(true) in
                    
                    UIView.animate(withDuration: 3, animations: {
                        
                        self.img_handshake.alpha = 1
                        self.img_team.alpha = 1
                    })
                }
            }
        }
    }
    
    @IBAction func GoHome(_ sender: UIButton) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController")
        self.present(nextViewController, animated: true, completion: nil)
    }
}
