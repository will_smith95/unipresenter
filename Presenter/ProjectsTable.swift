//
//  ProjectsTable.swift
//  Presenter
//
//  Created by Will Smith on 10/04/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit
import expanding_collection
import Cosmos

class ProjectsTable: ExpandingTableViewController {

    @IBOutlet weak var navBar: UINavigationItem!
    @IBOutlet weak var btn_close: UIBarButtonItem!
    
    var projectSkills: [ProjectSkillsObject] = []
    var skillsGained: [String] = []
    var skillsUsed: [(String,Int)] = []
    
    fileprivate var scrollOffsetY: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let skillsNib = UINib(nibName: "ProjectSkillsHeader", bundle: nil)
        tableView.register(skillsNib, forHeaderFooterViewReuseIdentifier: "ProjectSkillsHeader")
        
        let descriptionNib = UINib(nibName: "ProjectDescriptionHeader", bundle: nil)
        tableView.register(descriptionNib, forHeaderFooterViewReuseIdentifier: "ProjectDescriptionHeader")
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(closeProject), name: Notification.Name("CloseProject"), object: nil)
        
        configureSkills()
        
        
    }
    
    func closeProject() {
        
        popTransitionAnimation()
    }
    
    func configureSkills(){
        
        var skills: ProjectSkillsObject = ProjectSkillsObject.init()
        
        let openProject: String = UserDefaults.standard.value(forKey: "OpenProject") as! String
        
        switch openProject {
            
            case "Web Console":
                
                skills = ProjectSkillsObject(projectName: openProject, description: "VB.NET created software, added to bespoke company wide program used to complete orders placed on the Pricecheck website. 'Web Console' was my first big project and is used daily by the Sales Support team.", skillsGained: ["Logical Problem Solving", "Project Prioritising", "Time Management", "VB.NET"], skillsUsed: [("Communication" , 3), ("Problem Solving" , 4), ("Research" , 2), ("Data Management" , 5)])
            
            case "Pick Program":
                
                skills = ProjectSkillsObject(projectName: openProject, description: "Software created with VB.NET, used in the Warehouse to aid with picking products for customer and pricecheck staff orders.", skillsGained: ["MS SQL", "Visual Studio Frameworks", "Troubleshooting", "Program Distribution"], skillsUsed: [("Database Stored Procedures" , 3), ("VB.NET" , 5), ("Technical Support" , 1), ("Debugging Code" , 4)])
            
            case "Sales Presenter":
                
                skills = ProjectSkillsObject(projectName: openProject, description: "Swift written iOS application in progress going to be used by the Sales team to support daily jobs, arrange meetings with customers, and increase sales productivity.", skillsGained: ["API's", "Project Initiative", "Git/Source Control", "GUI Design"], skillsUsed: [("Adaptive Learning" , 3), ("Illustrator Design" , 4), ("Team Work" , 5), ("Communication" , 4)])
            
            case "Warehouse Scanner":
                
                skills = ProjectSkillsObject(projectName: openProject, description: "Swift/Objective C developed iOS application used at trade shows, future development will allow the warehouse to scan products during picking to increase efficiency.", skillsGained: ["Implementing Security", "Swift 3", "Xcode Environment", "Working To Deadline"], skillsUsed: [("Maths/Logic" , 2), ("Hardware Maintenance" , 4), ("Research" , 5), ("SQL" , 3)])
            default:
            
                break
        }
        
        projectSkills.append(skills)
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let colour = getColour()
        
        self.tableView.backgroundColor = colour

        if section == 0 {
            
            let header: ProjectDescriptionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProjectDescriptionHeader") as! ProjectDescriptionHeader
            
            header.view_background.backgroundColor = colour
            
            return header
        }else{
            
            let header: ProjectSkillsHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProjectSkillsHeader") as! ProjectSkillsHeader
            
            header.view_background.backgroundColor = colour
            
            return header
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 75
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        }else{

            skillsGained = projectSkills[0].skillsGained
            skillsUsed = projectSkills[0].skillsUsed
            
            return skillsGained.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let colour = getColour()
        let settingColour = getTheme()
        
        if indexPath.section == 0 {
            
            let cell: ProjectDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "projectDescriptionCell", for: indexPath) as! ProjectDescriptionCell
        
            cell.lbl_description.text = projectSkills[0].description
            cell.view_background.backgroundColor = colour
            cell.view_background.alpha = 0.5
            
            cell.alpha = 0
            
            UIView.animate(withDuration: 1, animations: {cell.alpha = 1})
            return cell
        }else{
            
            let cell: ProjectTableCell = tableView.dequeueReusableCell(withIdentifier: "projectTableCell", for: indexPath) as! ProjectTableCell
            
            cell.lbl_skillsGained.text = skillsGained[indexPath.row]
            cell.lbl_skillsUsed.text = skillsUsed[indexPath.row].0
            cell.star_skillsUsed.rating = Double(skillsUsed[indexPath.row].1)
            
            cell.view_background.backgroundColor = colour
            cell.view_background.alpha = 0.5
            
            cell.star_skillsUsed.backgroundColor = UIColor.clear
            
            cell.star_skillsUsed.settings.emptyBorderColor = settingColour
            cell.star_skillsUsed.settings.filledBorderColor = settingColour
            cell.star_skillsUsed.settings.filledColor = settingColour
                
            cell.lbl_skillsGained.sizeToFit()
            cell.lbl_skillsUsed.sizeToFit()
            
            cell.alpha = 0
            
            UIView.animate(withDuration: 1, animations: {cell.alpha = 1})
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 0
        
        if indexPath.section == 0 {
            
            height = 150
        }else{
            
            height = 100
        }
        
        return height
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func getColour() -> UIColor {
        
        var colour: UIColor = UIColor.white
        
        switch projectSkills[0].projectName {
            
        case "Pick Program":
            colour = UIColor(averageColorFrom: #imageLiteral(resourceName: "PickWide"))
            
        case "Web Console":
            colour = UIColor(averageColorFrom: #imageLiteral(resourceName: "WebWide"))
            
        case "Sales Presenter":
            colour = UIColor(averageColorFrom: #imageLiteral(resourceName: "PricecheckWide"))
            
        case "Warehouse Scanner":
            colour = UIColor(averageColorFrom: #imageLiteral(resourceName: "WarehouseWide"))
            
        default:
            break
        }
        
        return colour
    }
    
    func getTheme() -> UIColor {
        
        var colour: UIColor = UIColor.white
        
        if UserDefaults.standard.value(forKey: "SettingsToggle") as! String == "Blue" {
            
            colour = UIColor(colorWithHex: 0x395DA7)
        }else{
            
            colour = UIColor(colorWithHex: 0xB70D50)
        }
        
        return colour
    }
}

struct ProjectSkillsObject {
    
    var projectName: String = ""
    var description: String = ""
    var skillsGained: [String] = []
    var skillsUsed: [(String,Int)] = []
}

class ProjectDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var view_background: UIView!
}

class ProjectTableCell: UITableViewCell {
    
    @IBOutlet weak var lbl_skillsGained: UILabel!
    @IBOutlet weak var lbl_skillsUsed: UILabel!
    @IBOutlet weak var star_skillsUsed: CosmosView!
    @IBOutlet weak var view_background: UIView!
}

class ProjectSkillsHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_skillsGained: UILabel!
    @IBOutlet weak var lbl_skillUsed: UILabel!
    @IBOutlet weak var lbl_impact: UILabel!
}

class ProjectDescriptionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_close: UIButton!
    
    @IBAction func close() {
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("CloseProject"), object: nil)
    }
}
