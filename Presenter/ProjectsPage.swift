//
//  ProjectsPage.swift
//  Presenter
//
//  Created by Will Smith on 01/04/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit
import expanding_collection
import ChameleonFramework

class ProjectsPage: ExpandingViewController {
    
    @IBOutlet weak var btn_home: UIButton!
    @IBOutlet weak var lbl_projectTitle: UILabel!
    
    typealias ItemInfo = (imageName: String, title: String, language: String, difficulty: Double)
    
    fileprivate var cellsIsOpen = [Bool]()
    fileprivate let items: [ItemInfo] = [("WebTile", "", "VB.NET", 2),("PickTile", "", "VB.NET", 1),("PricecheckTile", "", "Swift", 4),("WarehouseTile", "", "Swift", 3)]
    
    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var img_background: UIImageView!
    
    @IBAction func GoHome() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController")
        self.present(nextViewController, animated: true, completion: nil)
    }
}

extension ProjectsPage {
    
    override func viewDidLoad() {
        
        itemSize = CGSize(width: 350, height: 400)
        super.viewDidLoad()
        
        registerCell()
        fillCellIsOpeenArry()
        addGestureToView(collectionView!)
    
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        SetBackground()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        collectionView?.reloadData()
    }
    
    func SetBackground() {
        
        if UserDefaults.standard.value(forKey: "SettingsToggle") as! String == "Blue" {
            
            img_background.image = UIImage(named: "PricecheckBackground")
        }else{
            
            img_background.image = UIImage(named: "HallamBackground")
        }
    }
}

extension ProjectsPage {
    
    fileprivate func registerCell() {
        
        let nib = UINib(nibName: String(describing: ProjectsCell.self), bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: ProjectsCell.self))
    }
    
    fileprivate func fillCellIsOpeenArry() {
        
        for _ in items {
            
            cellsIsOpen.append(false)
        }
    }
    
    fileprivate func getViewController() -> ExpandingTableViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let toViewController: ProjectsTable = storyboard.instantiateViewController(withIdentifier: "ProjectsTable") as! ProjectsTable
        
        return toViewController
    }
}

/// MARK: Gesture

extension ProjectsPage {
    
    fileprivate func addGestureToView(_ toView: UIView) {
        
        let gesutereUp = Init(UISwipeGestureRecognizer(target: self, action: #selector(ProjectsPage.swipeHandler(_:)))) {
            
            $0.direction = .up
        }
        
        let gesutereDown = Init(UISwipeGestureRecognizer(target: self, action: #selector(ProjectsPage.swipeHandler(_:)))) {
            
            $0.direction = .down
        }
        
        toView.addGestureRecognizer(gesutereUp)
        toView.addGestureRecognizer(gesutereDown)
    }
    
    func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        
        let indexPath = IndexPath(row: currentIndex, section: 0)
        
        guard let cell  = collectionView?.cellForItem(at: indexPath) as? ProjectsCell else { return }
        
        if cell.isOpened == true && sender.direction == .up {
            
            if cell.backgroundImageView.image == UIImage(named: "PricecheckTile") {
                
                UserDefaults.standard.set("Sales Presenter", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "PricecheckWide")
            }else if cell.backgroundImageView.image == UIImage(named: "WarehouseTile") {
                
                UserDefaults.standard.set("Warehouse Scanner", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "WarehouseWide")
            }else if cell.backgroundImageView.image == UIImage(named: "PickTile") {
                
                UserDefaults.standard.set("Pick Program", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "PickWide")
            }else if cell.backgroundImageView.image == UIImage(named: "WebTile") {
                
                UserDefaults.standard.set("Web Console", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "WebWide")
            }
        
            pushToViewController(getViewController())
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                
                rightButton.animationSelected(true)
            }
        }
        
        let open = sender.direction == .up ? true : false
        cell.cellIsOpen(open)
        cellsIsOpen[(indexPath as NSIndexPath).row] = cell.isOpened
    }
}

// MARK: UIScrollViewDelegate

extension ProjectsPage {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageNumber: Int = currentIndex + 1
        
        pageLabel.text = "\(pageNumber)/\(items.count)"
    }
}

// MARK: UICollectionViewDataSource

extension ProjectsPage {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        
        guard let cell = cell as? ProjectsCell else { return }
        
        let index = (indexPath as NSIndexPath).row % items.count
        let info = items[index]
        
        cell.lbl_projectTitle.text = info.title
        cell.lbl_projectType.text = info.language
        cell.lbl_difficulty.rating = info.difficulty
        
        if info.imageName == "PricecheckTile" {
            
            cell.backContainerView.backgroundColor = UIColor(averageColorFrom: UIImage(named: "PricecheckTile"))
        }else if info.imageName == "WarehouseTile" {
            
            cell.backContainerView.backgroundColor = UIColor(averageColorFrom: UIImage(named: "WarehouseTile"))
        }else if info.imageName == "PickTile" {
            
            cell.backContainerView.backgroundColor = UIColor(averageColorFrom: UIImage(named: "PickTile"))
        }else if info.imageName == "WebTile" {
            
            cell.backContainerView.backgroundColor = UIColor(averageColorFrom: UIImage(named: "WebTile"))
        }


        switch info.difficulty {
            
        case 1:
            cell.lbl_difficulty.settings.filledColor = UIColor.flatSkyBlue()
            cell.lbl_difficultyName.text = "Easy"
            
        case 2:
            cell.lbl_difficulty.settings.filledColor = UIColor.flatGreen()
            cell.lbl_difficultyName.text = "Comfortable"

        case 3:
            cell.lbl_difficulty.settings.filledColor = UIColor.flatYellow()
            cell.lbl_difficultyName.text = "Concentration Required"
            
        case 4:
            cell.lbl_difficulty.settings.filledColor = UIColor.flatOrange()
            cell.lbl_difficultyName.text = "Challenging"
            
        case 5:
            cell.lbl_difficulty.settings.filledColor = UIColor.flatRed()
            cell.lbl_difficultyName.text = "Difficult"
            
        default:
            break
        }
        
        cell.backgroundImageView.image = UIImage(named: info.imageName)
        
        cell.cellIsOpen(cellsIsOpen[index], animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? ProjectsCell
            , currentIndex == (indexPath as NSIndexPath).row else { return }
        
        if cell.isOpened == false {
            
            cell.cellIsOpen(true)
        } else {
            
            if cell.backgroundImageView.image == UIImage(named: "PricecheckTile") {
                
                UserDefaults.standard.set("Sales Presenter", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "PricecheckWide")
            }else if cell.backgroundImageView.image == UIImage(named: "WarehouseTile") {
                
                UserDefaults.standard.set("Warehouse Scanner", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "WarehouseWide")
            }else if cell.backgroundImageView.image == UIImage(named: "PickTile") {
                
                UserDefaults.standard.set("Pick Program", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "PickWide")
            }else if cell.backgroundImageView.image == UIImage(named: "WebTile") {
                
                UserDefaults.standard.set("Web Console", forKey: "OpenProject")
                cell.backgroundImageView.image = UIImage(named: "WebWide")
            }
            
            pushToViewController(getViewController())
            
            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
                rightButton.animationSelected(true)
            }
        }
    }
}

// MARK: UICollectionViewDataSource
extension ProjectsPage {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ProjectsCell.self), for: indexPath)
    }
}
