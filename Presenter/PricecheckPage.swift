//
//  PricecheckPage.swift
//  Presenter
//
//  Created by Will on 01/04/2017.
//  Copyright © 2017 Will. All rights reserved.
//

import UIKit

class PricecheckPage: UIViewController {

    @IBOutlet weak var pricecheckBackground: UIImageView!
    @IBOutlet weak var lbl_pricecheck: UILabel!
    @IBOutlet weak var img_pricecheck: UIImageView!
    @IBOutlet weak var lbl_role: UILabel!
    @IBOutlet weak var lbl_pricecheckTitle: UILabel!
    @IBOutlet weak var img_role: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbl_pricecheckTitle.alpha = 0
        self.lbl_pricecheck.alpha = 0
        self.img_pricecheck.alpha = 0
        self.lbl_role.alpha = 0
        self.img_role.alpha = 0
        
        self.navigationController?.isNavigationBarHidden = true
        
//        if UserDefaults.standard.value(forKey: "SettingsToggle") as! String == "Blue" {
//            
//            pricecheckBackground.image = UIImage(named: "PricecheckBackground")
//            //navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
//            //navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
//        }else{
//            
//            pricecheckBackground.image = UIImage(named: "HallamBackground")
//           // navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
//            //navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if UserDefaults.standard.value(forKey: "SettingsToggle") as! String == "Blue" {
            
            pricecheckBackground.image = UIImage(named: "PricecheckBackground")
            //navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
            //navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0x395DA7)
        }else{
            
            pricecheckBackground.image = UIImage(named: "HallamBackground")
            // navigationItem.rightBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
            //navigationItem.leftBarButtonItem?.tintColor = UIColor(colorWithHex: 0xB70D50)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 1, animations: {
            
            self.lbl_pricecheckTitle.alpha = 1
        }) {(true) in
                
            UIView.animate(withDuration: 1, animations: {
                
                self.lbl_pricecheck.alpha = 1
                self.img_pricecheck.alpha = 1
            }){(true) in
                
                UIView.animate(withDuration: 1, animations: {
                    
                    self.lbl_role.alpha = 1
                    self.img_role.alpha = 1
                })
            }
        }
    }
}
